
const bankBalanceDOM = document.getElementById("bankBalance")
const loanButton = document.getElementById("loanButton")
const payBalanceDOM = document.getElementById("payAmount")
const bankButton = document.getElementById("bankButton")
const workButton = document.getElementById("workButton")
const selectComputers = document.getElementById("laptops")
const features = document.getElementById("features")
const selName = document.getElementById("selectedName")
const selDescription = document.getElementById("selectedDescription")
const selPrice = document.getElementById("selectedPrice")
const selImage = document.getElementById("selectedImage")
const buyButton = document.getElementById("buyButton")
const infoDiv = document.getElementById("infoDiv")
const infoText = document.getElementById("infoText")
const dismissButton = document.getElementById("dismissButton")

let bankBalance = 0
let payBalance = 0
let gotLoan = false

const computers = [
    {
        "name": "Antel Ciluran E1",
        "price": 1200,
        "description": "This is a computer sometimes",
        "features": ["Has a screen", "Might have a mouse"],
        "image": "../images/comp1.png"
    },
    {
        "name": "Razen Treddripper U2",
        "price": 1500,
        "description": "Unlimited power",
        "features": ["Poor power", "Comes with bubble wrap"],
        "image": "../images/comp2.png"
    },
    {
        "name": "Notel hehe W3",
        "price": 2000,
        "description": "When you cant afford a Hotel",
        "features": ["Color is bright red", "Has a brother somewhere"],
        "image": "../images/comp3.png"
    },
    {
        "name": "Very Good Computer X4",
        "price": 700,
        "description": "This is a very good computer",
        "features": ["The computer might randomly shut down"],
        "image": "../images/comp4.png"
    }
]

let currentComputer = computers[0]


const renderDOM = () => {
    const kr = " kr"
    payBalanceDOM.innerHTML = payBalance + kr
    bankBalanceDOM.innerHTML = bankBalance + kr
    features.innerHTML = ""
    currentComputer.features.forEach(f => {
        const p = document.createElement("p")
        p.innerText = f
        features.appendChild(p)
    })
    selName.innerHTML = currentComputer.name
    selDescription.innerHTML = currentComputer.description
    selImage.src = currentComputer.image
    selPrice.innerHTML = currentComputer.price + kr
}

const work = () => {
    payBalance += 100
    renderDOM()
}

const addToBank = () => {

    if (payBalance === 0) showMessage("Do some work first!", "red")
    else {
        bankBalance += payBalance
        payBalance = 0
        showMessage("Pay added to bank!", "green")
        renderDOM()
    }
}

const getLoan = () => {

    let amount = parseInt(window.prompt("How much would you like to loan?"))
    if (isNaN(amount)) showMessage("Input must be a number!", "red")
    else if (gotLoan) showMessage("You allready got a loan for this computer!", "red")
    else if (amount <= 0) showMessage("Loan must be a positive number!", "red")
    else if (amount > bankBalance * 2) showMessage("Loan cant be more than double the amount of funds", "red")
    else {
        bankBalance += amount
        gotLoan = true
        showMessage("Yay! You got a loan!", "green")
        renderDOM()
    }
}

const changeSelectedComputer = (e) => {

    currentComputer = computers[e.target.selectedIndex]
    renderDOM()
}

const buyComputer = () => {

    if (bankBalance >= currentComputer.price) {
        bankBalance -= currentComputer.price
        gotLoan = false
        showMessage("Gratz! You have a new computer!", "green")
        renderDOM()
    }
    else {
        showMessage("Too poor!", "red")
    }
}

const showMessage = (message, color) => {

    infoText.innerText = message
    infoDiv.style.visibility = "visible"
    dismissButton.style.visibility = "visible"
    infoDiv.style.backgroundColor = color
}

const hideMessage = () => {
    
    infoDiv.style.visibility = "hidden"
    dismissButton.style.visibility = "hidden"
}


workButton.addEventListener("click", work)
bankButton.addEventListener("click", addToBank)
loanButton.addEventListener("click", getLoan)
selectComputers.addEventListener("change", changeSelectedComputer)
buyButton.addEventListener("click", buyComputer)
dismissButton.addEventListener("click", hideMessage)

renderDOM()

for (let i = 0; i < computers.length; i++) {
    option = document.createElement('option');
    option.setAttribute('value', computers[i].name);
    option.text = computers[i].name
    selectComputers.appendChild(option);
}